from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return HttpResponse('Inicio')

def categorias(request):
    contexto = {
        'nombre_autor': 'Jack Nicholson',
        'lista_categorias': [
            {'id': 1, 'nombre': 'acción', 'puntaje': 4},
            {'id': 2, 'nombre': 'sci-fi', 'puntaje': 2},
            {'id': 3, 'nombre': 'terror', 'puntaje': 1},
            {'id': 4, 'nombre': 'romance', 'puntaje': 5},
        ]
    }

    return render(request, 'catalogo/categorias.html', contexto)

def categoria(request, id):
    contexto = {'identificador': id}
    return render(request, 'catalogo/category.html', contexto)
